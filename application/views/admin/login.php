<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/img/leap.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/leap-logo-only.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>LEAP, Inc.</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>

	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="<?=base_url()?>assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/nucleo-icons.css" rel="stylesheet">

</head>
<body>
    <nav class="navbar navbar-expand-md fixed-top navbar-transparent">
        <div class="container">
			<div class="navbar-translate">
	            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
	            </button>
	            <a class="navbar-brand" href="<?=base_url()?>home">Leap, Inc.</a>
			</div>
			<div class="collapse navbar-collapse" id="navbarToggler">
	            <ul class="navbar-nav ml-auto">
					<li class="nav-item">
                        <a href="<?=base_url()?>home" class="nav-link">Home</a>
                    </li>
	                <li class="nav-item">
                        <a href="<?=base_url()?>about" class="nav-link">About Us</a>
                    </li>
					<li class="nav-item dropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle">Media </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-leap">
                            <a class="dropdown-item" href="sections.html#features">&nbsp; Blogs</a>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url()?>sign_up" class="nav-link">Sign Up</a>
                    </li>
                    <!--<li class="nav-item dropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle"><i class="nc-icon nc-book-bookmark"></i>Sign Up </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-info">
                            <a class="dropdown-item" href="sections.html#headers">&nbsp; Membership Fee </a>
                            <a class="dropdown-item" href="sections.html#features">&nbsp; Privileges </a>
                        </ul>
                    </li>-->
                    <li class="nav-item">
                        <a href="https://www.facebook.com/Literature-Educators-Association-of-the-Philippines-LEAP-Inc-1891414111114100/" target="_blank" class="nav-link">Contact Us</a>
                    </li>
	            </ul>
	        </div>
		</div>
    </nav>
    <div class="wrapper">
        <div class="page-header" style="background-image: url('<?=base_url()?>assets/img/meeting.jpg');">
            <div class="filter"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 ml-auto mr-auto">
                            <div class="card card-register">
                                <h3 class="title">Leap Board Login</h3>
								
                                <form action="" class="register-form" method="POST">
                                    <label >Email</label>
                                    <input type="email" class="form-control" placeholder="Email" name="email_address" id="email_address" required>
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                                    <button class="btn btn-outline-leap btn-block btn-round login-btn">Login</button>
                                </form>
                                <div class="forgot">
                                    <a href="#" class="btn btn-link btn-outline-leap">Forgot password?</a>
                                </div>
                            </div>
                        </div>

                    </div>
                   
					<div class="footer register-footer text-center">
						<h6>&copy; <script>document.write(new Date().getFullYear())</script> made by  by Creative Philipians 4:13</h6>
					</div>
                </div>  
          
        </div>
    </div>
    <div class="modal fade" id="EmailErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLabel">Sign-Up Failed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="Close-button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">Email is already existing. Please pick another email.
                </div>
                <div class="modal-footer">
                    <div class="right-side">
                        <button type="button" data-dismiss="modal" class="close btn btn-danger btn-link" id="got-it-button">Got it</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>

<!-- Core JS Files -->
<script src="<?=base_url()?>assets/js/jquery-3.2.1.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/popper.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="<?=base_url()?>assets/js/paper-kit.js?v=2.1.1"></script>

<script src="<?=base_url()?>assets/js/login.js?v=1.0.0"></script>


   

</html>
