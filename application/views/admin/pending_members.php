<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Pending Members</h4>
            <p class="card-category"> </p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class="text-primary">
                  <th>
                    Name
                  </th>
                  <th>
                    Sex
                  </th>
                  <th>
                   Birthday
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Permanent address
                  </th>
                  <th>
                    Residence contact number
                  </th>
                  <th>
                    Office contact number
                  </th>
                  <th>
                    Send Membership to
                  </th>
                  <th>
                    
                  </th>
                </thead>
                <tbody>
                  <?php foreach($pending_members->result() as $pending_member) {  ?>
                    <tr>
                      <td><?=$pending_member->fullname?></td>
                      <td><?=$pending_member->gender?></td>
                      <td><?=date("F j, Y",strtotime($pending_member->birthday))?></td>
                      <td><?=$pending_member->email_address?></td>
                      <td><?=$pending_member->address?></td>
                      <td><?=$pending_member->residence_number?></td>
                      <td><?=$pending_member->office_number?></td>
                      <td><?=$pending_member->send_membership?></td>
                      <td><a href="<?=base_url()?>user/approve_member/<?=$pending_member->user_id?>">APPROVE</a></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>