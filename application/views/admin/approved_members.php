<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Approved Members</h4>
            <p class="card-category"> </p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class="text-primary">
                  <th>
                    Name
                  </th>
                  <th>
                    Sex
                  </th>
                  <th>
                   Birthday
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Permanent address
                  </th>
                  <th>
                    Residence contact number
                  </th>
                  <th>
                    Office contact number
                  </th>
                  <th>
                    Date Approved
                  </th>
                  <th>
                    
                  </th>
                </thead>
                <tbody>
                  <?php foreach($approved_members->result() as $approved_member) {  ?>
                    <tr>
                      <td><?=$approved_member->fullname?></td>
                      <td><?=$approved_member->gender?></td>
                      <td><?=date("F j, Y",strtotime($approved_member->birthday))?></td>
                      <td><?=$approved_member->email_address?></td>
                      <td><?=$approved_member->address?></td>
                      <td><?=$approved_member->residence_number?></td>
                      <td><?=$approved_member->office_number?></td>
                      <td><?=$approved_member->date_approved?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>