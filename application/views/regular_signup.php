<div class="main">
  <div class="features-1">
    <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">How do i become a Regular Member?</h2>
            <h5 class="description">Here are the steps for becoming a regular member, kindly check each number so the processing of your membership will become easy.</h5>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-check-2"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Check</h4>
                        <p class="description">Check if you're eligible<br>
                        Interested in the advancement<br> of linguistics and language education as a science and as a profession? If yes, proceed.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-money-coins"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Deposit</h4>
                        <p>Deposit Php 500/year to any Landbank of the Philippines<br>
                          <b>Account name:</b> Literature Educators Association of the Philippines-LEAP, Inc. <br>
                          Account Number: 1981-1731-60
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-camera-compact"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Scan</h4>
                        <p>Scan or take a clear picture of the deposit slip/receipt.<br>
                        send proof of payment to <h5><b>leaphilippines.inc@gmail.com</b></h5></p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-paper"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Fill out</h4>
                        <p>Fill out the online registration form by here. <br>
                        or renew your membership by clicking here.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-watch-time"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Wait</h4>
                        <p>Wait for the confirmation e-mail and the membership e-certificate. It will be sent by the LEAP Secretariat within 7-14 working days.  </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-single-02"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Review</h4>
                        <p>Review your information via MTS. Click the icon above to track, view, correct, or update your current membership information.  </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Additional courier fee of P100 (NCR & GMA) and P150 (rest of the country)</h2>
          </div>
        </div>
        <div class="col-md-12">
            <form class="contact-form container" method="POST" action="<?=base_url()?>user/register_member">
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>First Name</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="firstname" class="form-control" placeholder="Juan" required>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Middle Name</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="middlename" class="form-control" placeholder="Pedro" required="">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Last Name</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="lastname" class="form-control" placeholder="Dela Cruz" required>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto text-center">
                  <div class="form-group">
                      <select class="selectpicker" data-style="btn btn-default" name="gender" required>
                          <option disabled selected> Gender </option>
                          <option value="Male">Male </option>
                          <option value="Female">Female</option>
                     </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto text-center">
                  <div class="title">
                    <h5>Birthdate</h5>
                  </div>
                  <div class="row">
                      <div class="col-sm-6 ml-auto mr-auto">
                          <div class="form-group">
                              <div class='input-group date' id='datetimepicker'>
                                  <input type='text' class="form-control datetimepicker" placeholder="06/07/2017" name="birthdate" required />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Email</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-email-85"></i>
                          </span>
                          <input type="text" name="email_address" class="form-control" placeholder="Email" required>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Highest degree attained (Please put the full title of the degree e.g. Master of Arts in Education, Specialization in Literature [English Stream].) </label>
                       <textarea class="form-control border-input" placeholder="" rows="5" name="highest_degree" required></textarea>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>School/Institution and its complete address  </label>
                       <textarea class="form-control border-input" placeholder="" rows="5" name="school" required></textarea>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Current position (e.g., Master Teacher I, Special Science Teacher II, Instructor III, etc.)   </label>
                       <textarea class="form-control border-input" placeholder="" rows="5" name="job_position" required></textarea>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                  <div class="title">
                    <h4>Grade level/s taught</h4>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="pkgr3" value="Pre-kinder to Grade 3" >
                        Pre-kinder to Grade 3
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="gr4gr6" value="Grade 4 to 6" >
                        Grade 4 to 6
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="gr7gr10" value="Grade 7 to 10" >
                        Grade 7 to 10
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="gr11gr12" value=" Grade 11 to 12" >
                        Grade 11 to 12
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="college" value="College" >
                        College
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="masters" value="Master's Level" >
                        Master's Level
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="doctorate" value="Doctorate Level" >
                        Doctorate Level
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="witheduc" value="With Education degree but not teaching" >
                        With Education degree but not teaching
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="notfinish" value="Did not finish undergraduate degree" >
                        Did not finish undergraduate degree
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="taught_level" id="other_radio" value="Other">
                        Other
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="row">
                  <div class="col-md-12 ml-auto mr-auto">
                       <textarea class="form-control border-input" placeholder="" rows="5"  id="textarea_other" name="other_taught_level"></textarea>
                  </div>
              </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                  <div class="title">
                    <h4>Number of teaching years</h4>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="1to3" value="1-3 years" >
                        1-3 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="4to7" value="4-7 years" >
                        4-7 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="8to10" value="8-10 years" >
                        8-10 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="11to15" value="11-15 years" >
                        11-15 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="16to25" value="16-25 years" >
                        16-25 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="25plus" value="more than 25 years" >
                        more than 25 years
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="teaching_years" id="none" value="none" >
                        None
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mr-auto ml-auto">
                  <div class="title">
                    <h4>Areas of specialization in literature</h4>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[1]" value="Philippine Literature">
                          Philippine Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[2]" value="Asian Literature">
                          Asian Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[3]" value="African Literature">
                          African Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[4]" value="European Literature">
                          European Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[5]" value="American Literature">
                          American Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[6]" value="World Literature">
                          World Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[7]" value="Literary Criticism">
                          Literary Criticism
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization8[]" value="Literary Research">
                          Literary Research
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[9]" value="Literary History">
                          Literary History
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[10]" value="Creative Writing">
                          Creative Writing
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[11]" value="Language and Literacy">
                          Language and Literacy
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[12]" value="Teaching Literature">
                          Teaching Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[13]" value="Assessment in Literature">
                          Assessment in Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[14]" value="21st Century Literature">
                          21st Century Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[15]" value="Postcolonial Literature">
                          Postcolonial Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="specialization[16]" value="Postmodern Literature">
                          Postmodern Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="" id="specialization_other">
                          Other
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <textarea class="form-control border-input col-md-12" placeholder="" rows="5" style="visibility: hidden;"  id="specialization_textarea" name="specialization[17]"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mr-auto ml-auto">
                  <div class="title">
                    <h4>Areas of interest (I want to deepen my knowledge of this/these subject/s through LEAP activities.)</h4>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[1]" value="Philippine Literature">
                          Philippine Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[2]" value="Asian Literature">
                          Asian Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[3]" value="African Literature">
                          African Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[4]" value="European Literature">
                          European Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[5]" value="American Literature">
                          American Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[6]" value="World Literature">
                          World Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[7]" value="Literary Criticism">
                          Literary Criticism
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[8]" value="Literary Research">
                          Literary Research
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[9]" value="Literary History">
                          Literary History
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[10]" value="Creative Writing">
                          Creative Writing
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[11]" value="Language and Literacy">
                          Language and Literacy
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[12]" value="Teaching Literature">
                          Teaching Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[13]" value="Assessment in Literature">
                          Assessment in Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[14]" value="21st Century Literature">
                          21st Century Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[15]" value="Postcolonial Literature">
                          Postcolonial Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[16]" value="Postmodern Literature">
                          Postmodern Literature
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="interest[17]" id="interest_other">
                          Other
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <textarea class="form-control border-input col-md-12" placeholder="" rows="5" style="visibility: hidden;"  id="interest_textarea" name="interest_textarea"></textarea>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Permanent residence address</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="address" class="form-control" placeholder="(Home number, Street, Baranggay, City, Province, Country, Postal Code)">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Residence landline and/or mobile number</label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="residence_number" class="form-control" placeholder="02xxx-xx-xx or 09xxxxxxxxx">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <label>Office landline and/or mobile number </label>
                      <div class="input-group">
                          <span class="input-group-addon">
                              <i class="nc-icon nc-single-02"></i>
                          </span>
                          <input type="text" name="office_number" class="form-control" placeholder="02xxx-xx-xx or 09xxxxxxxxx">
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                  <div class="title">
                    <h4>Where would you want your membership and ID be sent?</h4>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="send_membership" id="home_address" value="Home address" maxlength="11" >
                        Home address
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                  <div class="form-check-radio">
                    <label class="form-check-label" style="margin-top: 0px !important">
                        <input class="form-check-input" type="radio" name="send_membership" id="office_address" value="Office Address" maxlength="11" >
                        Office Address
                        <span class="form-check-sign"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                    <label>Password</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="nc-icon nc-key-25"></i>
                        </span>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                </div>
              </div>
              <div class="row"  style="visibility: hidden !important">
                  <div class="col-md-12  ml-auto mr-auto">
                          <div class="text-center">
                              <select name="membership_type" class="selectpicker text-center col-md-6" data-style="btn-leap btn-round" data-menu-style="dropdown-info" >
                                <option value="1" selected>Regular </option>
                                <option value="2">Student</option>
                                <option value="3">Insitutional</option>
                                <option value="4">Lifetime </option>  
                              </select>
                          </div>       
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-3 ml-auto mr-auto">
                      <button class="btn btn-leap btn-lg btn-fill">Sign Up</button>
                  </div>
              </div>
          </form>
          </div>
        </div>  
    </div>
  </div>
</div>



