<div class="main">
  <div class="features-1">
    <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">How to become a Student Member?</h2>
            <h5 class="description">Here are the steps for becoming a student member, kindly check each number so the processing of your membership will become easy.</h5>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-check-2"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Check</h4>
                        <p class="description">Check if you're eligible<br>
                        Are you an enrolled undergraduate or graduate student interested or engaged in the advancement of linguistics and language education? If yes, proceed.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-money-coins"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Deposit</h4>
                        <p>Deposit Php 700/year  to any Bank of the Philippine Islands (BPI) branch.<br>

                        </p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-camera-compact"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Scan</h4>
                        <p>Scan or take a clear picture of the deposit slip/receipt and your student ID.<br>
                        take note of the transaction reference number of your deposit slip.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-paper"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Fill out</h4>
                        <p>Fill out the online registration form by here. <br>
                        or renew your membership by clicking here.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-watch-time"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Wait</h4>
                        <p>Wait for the confirmation e-mail and the membership e-certificate. It will be sent by the LEAP Secretariat within 7-14 working days.  </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-single-02"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Review</h4>
                        <p>Review your information via MTS. Click the icon above to track, view, correct, or update your current membership information.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  alert('Signup Complete, Please wait for Approval');
</script>