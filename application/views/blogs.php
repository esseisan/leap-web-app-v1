<div class="page-header" data-parallax="true" style="background-image: url('<?=base_url()?>assets/img/blog-pictures/blog-post-1.jpg');">
            <div class="filter"></div>
            <div class="container">
                <div class="motto text-center">
                    <h1 style="font-weight:500;font-size:4.6em">LEAP NCLE 2017</h1>
                    <h3>A Success</h3>
                    <br>

                </div>
            </div>
        </div>

        <div class="main">
          <div class="section text-center">
            <div class="container">
                <div class="row">
                  <div class="col-md-12 ml-auto mr-auto text-center">
                        <h5 class="" style="text-indent: 50px; text-align: justify;">The Literature Educators Association of the Philippines, Inc., also known as LEAP, Inc., officially and successfully launched and spearheaded their first ever National Conference in fulfillment of its strong commitment to the growth and development of literature educators in the country. Held last October 5-7, at no less than the Philippine Normal University, the conference, dubbed as the National Conference in Literature Education, featured some of the country premiere and esteemed literatis—writers and educators alike. With over a hundred and fifty (150) teacher participants from all over both the secondary and the tertiary level, the first LEAP-sponsored conference is a huge success.
                        </h5>
                  </div>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                  <div class="col-md-12 ml-auto mr-auto text-center">
                        <h5 class="" style="text-indent: 50px; text-align: justify;">Led by its driven and passionate president and founder, Dr. Jennie V. Jocson, whose influence as a literature teacher inspired many of her students, LEAP, her brainchild and a vision she has long seen in the horizon, finally came to fruition on a maiden meeting, November 19, 2016. Among the agreed plans was the said conduct of a national conference for literature teachers. 
                        </h5>
                  </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                  <div class="col-md-12 ml-auto mr-auto text-center">
                        <h5 class="" style="text-indent: 50px; text-align: justify;">Joined by respectable literature educators from various exclusive and state universities, LEAP, Inc., was the pioneering board of directors was born. Among the officers elected were:
                        </h5>
                  </div>
                </div>
            </div>
            <div class="container" style="margin-top: 30px">
                <div class="row">
                  <div class="col-md-4">
                    <h5>Jennie V.  Jocson, Ph.D. </h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>President</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Philippine Normal University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Erwin Oamil, Ph. D. </h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Vice President</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Philippine Normal University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Alona Guevarra, Ph.D.</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Secretary</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Ateneo De Manila University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Erly Parungao</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Assistant Secretary</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Philippine Normal University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Chris Wright</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Treasurer</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Philippine Normal University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Noemi Baysa</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Assistant Treasurer</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Department of Education-Division of Malabon</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Rafael Paz</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>PRO</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Polytechnic University of the Philippines</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Carlo Fernando</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Auditor</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Commission on Higher Education (CHED)</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Mark Muyano</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Board of Directors</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Central Luzon State University</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Mike Naydas</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Board of Directors</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Adventist University of the Philippines</h5> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <h5>Erhwin Clarin</h5>  
                  </div>
                  <div class="col-md-4">
                      <h5>Board of Directors</h5>
                  </div>
                  <div class="col-md-4">
                      <h5>Philippine Normal University</h5> 
                  </div>
                </div>
            </div>
            <div class="container" style="margin-top: 30px">
                <div class="row">
                  <div class="col-md-12 ml-auto mr-auto text-center">
                        <h5 class="" style="text-indent: 50px; text-align: justify;">The said officers were sworn into office on the first day of the national conference, October 5, 2017. 
                        </h5>
                  </div>
                </div>
            </div>
            <div class="container" style="margin-top: 30px">
              <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="card card-plain card-blog">                          
                      <div class="card-image">
                          <img class="img" src="assets/img/blog-pictures/blog-post-2.jpg">
                      </div>
                    </div>
                    <div class="">
                      Dr. Venancio L. Mendiola <br>
                      Plenary Speaker, LEAP- NCLE 2018
                    </div>
                </div>
                <div class="col-md-9">
                  <div style="text-indent: 50px; text-align: justify; font-size: 18px">
                    The first day of the 3-day conference was highlighted by the insightful contributions from two of PNU’s premiere literature educators, Prof. Ruth A. Alido for the Keynote Address and Dr. Venancio L. Mendiola as the Plenary Speaker. Prof. Alido’s keynote address emphasized the relevance of building on and bridging the K to 12 Literature teacher. This was reinforced by the powerful lecture given by the dynamic and zealous literature educator, Dr. Venancio L. Mendiola in the afternoon. His plenary speech focused on the intersection of the classics and contemporary reading materials, vis-à-vis the curriculum. This first day, October 5, is the day the world celebrates teachers and the teaching profession. The teacher-participants were given what seems like the most fitting tribute and reminder, to continue to hone their craft and nurture their profession. 
                  </div>
                </div>
              </div>
            </div>
            <div class="container" style="margin-top: 30px">
                <div class="row">
                  <div class="col-md-12 ml-auto mr-auto text-center">
                        <h5 class="" style="text-indent: 50px; text-align: justify;">The second day was rich with plenary sessions on various topics related to the teaching of 21st Century Philippine and World Literature, as well as some potential areas for literary research. The highlight of the day includes the most important names in literary pedagogy and literature writing, including the notable Palanca Award winner and UP professor, Ferdinand Pisigan-Jarin, Prof. Victor Rey Fumar (PNU), Dr. Alona U. Guevarra (ADMU), Prof. Marla C. Papango (PNU), Prof. Rafael O. Paz (PUP), Prof. Erhwin A. Clarin (PNU) and Dr. Michael S. Naidas (Adventist University of the Philippines), renowned writer, Mr. Marne Kilates, Ms. Ailil Alvarez of the UST Publishing House. Each panel was capped with very interactive open fora. 
                        </h5>
                  </div>
                </div>
            </div>
            <div class="container" style="margin-top: 30px">
              <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="card card-plain card-blog">                          
                      <div class="card-image">
                          <img class="img" src="assets/img/blog-pictures/blog-post-3.jpg">
                      </div>
                    </div>
                    <div class="">
                     Dr. Cristina Pantoja-Hidalgo <br>
                      Plenary Speaker, LEAP- NCLE 2018
                    </div>
                </div>
                <div class="col-md-9">
                  <div style="text-indent: 50px; text-align: justify; font-size: 18px">
                   Day 2 was also graced by the presence of UST’s Director for Creative Writing, an established writer herself, Dr. Cristina Pantoja-Hidalgo. Dr. Hidalgo shared some of the most effective pedagogies that teachers could use in their classrooms, be it in their literary discussions or in literary productions. Her session did well to stimulate more innovative ideas to use literature in the teacher-participants’ classrooms.
                  </div>
                  <br>
                  <br>
                  <div style="text-indent: 50px; text-align: justify; font-size: 18px">
                  The third day was a half-day business that further explored some of the most controversial issues and subjects in 21st century literature, e.g., racism, gender, diaspora, etc.   Panelists for day 3 include Prof. Christopher Y. Wright of National University (NU) and Prof. Erly S. Parungao of PNU, as well as Mr. Kornellie Raquitico of Philippine Science High School. LEAP Vice President, Dr. Erwin L. Oamil also informed the participants of the future plans and directions of LEAP in the conference’s 3rd Keynote Speech.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
            
            <div class="section landing-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="text-center">What are you waiting for? Sign Up!</h2>
                            <!--<form class="contact-form" method="POST" action="<?=base_url()?>user/register_member">
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>First Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-single-02"></i>
                                            </span>
                                            <input type="text" name="firstname" class="form-control" placeholder="Juan">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Last Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-single-02"></i>
                                            </span>
                                            <input type="text" name="lastname" class="form-control" placeholder="Dela Cruz">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-email-85"></i>
                                            </span>
                                            <input type="text" name="email_address" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-key-25"></i>
                                            </span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="visibility: hidden; display: none;">
                                    <div class="col-md-12  ml-auto mr-auto">
                                            <div class="text-center">
            
                                                <select name="membership_type" class="selectpicker text-center col-md-6" data-style="btn-leap btn-round" data-menu-style="dropdown-info">
                                                    <option disabled > Pick membership</option>
                                                    <option value="1" selected>Regular </option>
                                                    <!--<option value="2">Student</option>
                                                    <option value="3">Insitutional</option>
                                                    <option value="4">Lifetime </option>  -->  
                                                </select>
                                            </div>  
                                           
                                    </div>
                                </div>
                                
                                
                            </form>-->
                            <div class="row">
                                <div class="col-md-1 ml-auto mr-auto">
                                    <a href="<?=base_url()?>sign_up/regular_sign_up" class="btn btn-leap btn-lg btn-fill">Sign Up</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
