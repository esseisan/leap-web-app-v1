<div class="page-header" data-parallax="true" style="background-image: url('<?=base_url()?>assets/img/meeting.jpg');">
			<div class="filter"></div>
			<div class="container">
			    <div class="motto text-center">
                    <img src="<?=base_url()?>assets/img/leap-logo-only.png">
			        <h1>Lorem ipsum dolor sit amet</h1>
			        <h3>Cras viverra quam quis eros sodales fermentum.</h3>
			        <br />
			        
			    </div>
			</div>
    	</div>
       
        <div class="main">
			<div class="section text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h2 class="title text-center">News & Updates</h2>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-plain card-blog text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="d-block img-fluid" src="assets/img/bruno-abatti.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-warning">Travel</h6>
                                <h3 class="card-title">
                                    <a href="#pablo">Learning different cultures through travel</a>
                                </h3>
                                <p class="card-description">
                                    A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot.
                                </p>
                                <br>
                                <a href="#pablo" class="btn btn-leap btn-round"> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-plain card-blog text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="d-block img-fluid" src="assets/img/bruno-abatti.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-info">Travel</h6>
                                <h3 class="card-title">
                                    <a href="#pablo">Kick-Ass ways to disappear like a Ninja!</a>
                                </h3>
                                <p class="card-description">
                                    In the end, the judge ruled that Levandowski could be brought in and examined, but that each question asked to him would be vetted in advance and should have some basis in evidence.
                                </p>
                                <br>
                                <a href="#pablo" class="btn btn-leap btn-round"> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-plain card-blog text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="d-block img-fluid" src="assets/img/bruno-abatti.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-danger">Lifestyle</h6>
                                <h3 class="card-title">
                                    <a href="#pablo">We will breathe clean air and exercise</a>
                                </h3>
                                <p class="card-description">
                                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is too high for the beams and angle of the ceiling...
                                </p>
                                <br>
                                <a href="#pablo" class="btn btn-leap btn-round"> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-plain card-blog text-center">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="d-block img-fluid" src="assets/img/bruno-abatti.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-success">Best Seller</h6>
                                <h3 class="card-title">
                                    <a href="#pablo">Readers Pick of The Month</a>
                                </h3>
                                <p class="card-description">
                                    “Raising equity is very expensive” In essence, it lets new consumer businesses apply to raise funding on its platform, and gives investors a new way to find and back those tricks to finance their growing businesses.
                                </p>
                                <br>
                                <a href="#pablo" class="btn btn-leap btn-round"> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="section section-dark text-center">
            <div class="container">
                <h2 class="title">President's Corner</h2>
				<div class="row">
    				

    				

    				<div class="col-md-12">
                        <div class="card card-profile card-plain">
                            <div class="card-avatar">
                                <a href="#avatar"><img src="<?=base_url()?>assets/img/faces/erik-lucatero-2.jpg" alt="..."></a>
                            </div>
                            <div class="card-body">
                                <a href="#paper-kit">
                                    <div class="author">
                                        <h4 class="card-title">Robert Orben</h4>
                                        <h6 class="card-category">Developer</h6>
                                    </div>
                                </a>
                                <p class="card-description text-center">
                                The strength of the team is each individual member. The strength of each member is the team. If you can laugh together, you can work together, silence isn’t golden, it’s deadly.
                                </p>
                            </div>
                            <div class="card-footer text-center">
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
    				</div>
    			</div>
        	</div>
    	</div>

            <div class="section landing-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="text-center">What are you waiting for? Sign Up!</h2>
                            <form class="contact-form">
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Name</label>
										<div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="nc-icon nc-single-02"></i>
	                                        </span>
	                                        <input type="text" class="form-control" placeholder="Name">
	                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Email</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="nc-icon nc-email-85"></i>
											</span>
											<input type="text" class="form-control" placeholder="Email">
										</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-key-25"></i>
                                            </span>
                                            <input type="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 ml-auto mr-auto">
                                        <button class="btn btn-leap btn-lg btn-fill">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>