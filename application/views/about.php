<div class="page-header page-header-small" style="background-image: url('<?=base_url()?>assets/img/leap-org.png');">
    <div class="filter filter-danger"></div>
	<div class="content-center">
		<div class="motto container">
			<h1 class="text-center">About Us</h1>
			<h3>Literature Educators Association of the Philippines, Inc. </h3>
		</div>
	</div>
</div>
<div class="wrapper">
    <div class="main">
        <div class="section">
            <div class="container about-us">
                <h3 class="title-uppercase color-leap">About Us</h3>
                <p>Literature Educators Association of the Philippines-LEAP is a non-stock, non-profit, non-partisan and non-sectarian association of professional educators in the country.</p>

                <p>LEAP is established mainly for the development and advancement of literature teachers and educators in the country. LEAP is the first of its kind as it caters to the specific demands of, and addresses the growing trend and changing dynamic of literature teaching in the field, especially with the implementation of the K to 12 curriculum, as well as the shift to an outcomes-based and literature-centered classroom.</p>


                <p>The organization was born on November 2016, and was officially launched on December 2016, at the Philippine Normal University, the country’s National Center for Teacher Education. From a handful of 75 literature teachers, the organization continues to grow and spread across the country, creating a strong link for all literature teachers in the Philippines.</p>

                <h3 class="title-uppercase creators color-leap">Leap Purpose</h3>
                <p>Literature Educators Association of the Philippines-LEAP is established for the development and advancement of teachers and education. Specifically, the association aims to: </p>

                <p>
                    •   monitor trends, developments and researches in the field of literature education, and teacher education in general; 
                </p>
                <p>
                    •   disseminate and share research findings and insights on matters related to literature education and teacher education in general, through conventions, seminar-workshops, publications, and other professional activities; 
                </p>
                <p>                    
                    •   assist members in their personal and professional advancement through exposure to developments in the field of Teacher Education, and literature education in general. 

                </p>
                

                <h2 class="text-center creators color-leap">Board Directors</h2>
                <div class="row">


                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/mark.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    MARK ANTHONY G. MOYANO, Ph.D. <br>
                                    (Central Luzon State University)

                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/michael.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    MICHAEL S. NAIDAS, Ph.D. <br>
                                    (Adventist University of the Philippines)



                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/carmenchu2.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    CARMENCHU M. LACSAMANA, Ph.D. <br>
                                    (Assumption College)



                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/leap-logo-only.png" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    ERHWIN A. CLARIN, MAT <br>
                                    (Philippine Normal University)

                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/amanah.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    AMANAH FATIMA C. VILLAPANDO <br>
                                    (DepEd-Marikina)

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 class="text-center creators color-leap">Board Members</h2>
				<div class="row">
                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/jennie.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    JENNIE V. JOCSON, Ph.D. <br>
                                    (Philippine Normal University) <br>
                                    President
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/erwin.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    ERWIN L.  OAMIL, Ph.D. <br>
									(Philippine Normal University) <br>
									Vice President
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/erly.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    ERLY S. PARUNGAO, M.A. <br>
									(Philippine Normal University) <br>
									Secretary
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/kornellie.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    KORNELLIE L. RAQUITICO, M.A. <br>
									(Philippine Science High School) <br>
									Assistant Secretary
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/faces/mc.jpg" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    CHRISTOPHER Y. WRIGHT, M.A. <br>
									(National University) <br>
									Treasurer


                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/leap-logo-only.png" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    NOEMI S. BAYSA, M.A. <br>
									(DepEd-Malabon) <br>
									Assistant Treasurer


                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/leap-logo-only.png" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
                                    RAFAEL O. PAZ, M.A. <br>
									(Polytechnic University of the Philippines) <br>
									PRO
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card card-profile card-plain">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#avatar">
                                        <img src="<?=base_url()?>assets/img/leap-logo-only.png" alt="...">
                                    </a>
                                </div>
                                <p class="card-description text-center">
									JOHN CARLO P. FERNANDO <br>
									(CHED) <br>
									Auditor
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
