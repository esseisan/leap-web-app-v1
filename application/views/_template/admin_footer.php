<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, made by
      <a href="https://fb.me/svillwebsolutions" target="_blank">SV Web Solutions</a> .
    </div>
  </div>
</footer>