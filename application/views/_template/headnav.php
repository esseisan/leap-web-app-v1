 <nav class="navbar navbar-expand-lg fixed-top navbar-info bg-leap" color-on-scroll="300">
        <div class="container">
			<div class="navbar-translate">
				<a class="navbar-brand" href="<?=base_url()?>home">LEAP, Inc.</a>
	            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
					<span class="navbar-toggler-bar"></span>
	            </button>
			</div>
	        <div class="collapse navbar-collapse" id="navbarToggler">
	            <ul class="navbar-nav ml-auto">
					<li class="nav-item">
                        <a href="<?=base_url()?>home" class="nav-link">Home</a>
                    </li>

                    <li class="nav-item">
                        <a href="<?=base_url()?>about" class="nav-link">About Us</a>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle">Media </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-leap">
                            <!--<a class="dropdown-item" href="sections.html#headers">&nbsp;Youtube Videos of the NCLE Talks</a>-->
                            <a class="dropdown-item" href="<?=base_url()?>blogs">&nbsp; Blogs</a>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url()?>sign_up/overview" class="nav-link">Sign Up</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url()?>mts" class="nav-link">Membership Tracking</a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="#" class="nav-link">Research</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.facebook.com/Literature-Educators-Association-of-the-Philippines-LEAP-Inc-1891414111114100/?ref=br_rs" class="nav-link" target="_blank">Contact Us</a>
                    </li>
                    
	            </ul>
	        </div>
		</div>
    </nav>