<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?=base_url()?>assets/img/leap.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/leap-logo-only.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>LEAP, Inc.</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/paper-kit-v2.css?v=2.1.1" rel="stylesheet"/>
	<link href="<?=base_url()?>assets/css/paper-kit.css?v=2.1.1" rel="stylesheet"/>

	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="<?=base_url()?>assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/nucleo-icons.css" rel="stylesheet">

</head>
<body>
   <?php $this->load->view('_template/headnav')?>
        <?=$body?>
   <?php $this->load->view('_template/footer')?>
		
	
</body>

<!-- Core JS Files -->
<script src="<?=base_url()?>assets/js/jquery-3.2.1.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/popper.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap-select.js" type="text/javascript"></script>
<!--  Paper Kit Initialization snd functons -->
<script src="<?=base_url()?>assets/js/moment.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/paper-kit.js?v=2.1.1"></script>
<script src="<?=base_url()?>assets/js/registration.js?v=1.0.2"></script>

</html>
