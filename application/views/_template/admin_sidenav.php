<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Leap, Inc.
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?php if($link == 'admin/user_profile') {?> active <?php } ?>">
            <a class="nav-link" href="<?=base_url()?>admin/user_profile">
              <i class="material-icons">person</i>
              <p>User Profile</p>
            </a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="./dashboard.html">
              <i class="material-icons">dashboard</i>
              <p>News</p>
            </a>
          </li>
          <li class="nav-item <?php if($link == 'admin/pending_members') {?> active <?php } ?> ">
            <a class="nav-link" href="<?=base_url()?>admin/pending_members">
              <i class="material-icons">content_paste</i>
              <p>Pending Members List</p>
            </a>
          </li>
          <li class="nav-item <?php if($link == 'admin/approved_members') {?> active <?php } ?>">
            <a class="nav-link" href="<?=base_url()?>admin/approved_members">
              <i class="material-icons">content_paste</i>
              <p>Accepted Members List</p>
            </a>
          </li>
          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
      </div>
    </div>