<div class="main">
  <div class="features-1">
    <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">How do i become a Regular Member?</h2>
            <h5 class="description">Here are the steps for becoming a regular member, kindly check each number so the processing of your membership will become easy.</h5>
          </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-check-2"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Check</h4>
                        <p class="description">Check if you're eligible<br>
                        Interested in the advancement<br> of linguistics and language education as a science and as a profession? If yes, proceed.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-money-coins"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Deposit</h4>
                        <p>Deposit Php 500/year to any Landbank of the Philippines<br>
                          <b>Account name:</b> Literature Educators Association of the Philippines-LEAP, Inc. <br>
                          Account Number: 1981-1731-60
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-camera-compact"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Scan</h4>
                        <p>Scan or take a clear picture of the deposit slip/receipt.<br>
                        send proof of payment to <h5><b>leaphilippines.inc@gmail.com</b></h5></p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-paper"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Fill out</h4>
                        <p>Fill out the online registration form by here. <br>
                        or renew your membership by clicking here.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-watch-time"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Wait</h4>
                        <p>Wait for the confirmation e-mail and the membership e-certificate. It will be sent by the LEAP Secretariat within 7-14 working days.  </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info">
                    <div class="icon icon-danger">
                        <i class="nc-icon nc-single-02"></i>
                    </div>
                    <div class="description">
                        <h4 class="info-title">Review</h4>
                        <p>Review your information via MTS. Click the icon above to track, view, correct, or update your current membership information.  </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Additional courier fee of P100 (NCR & GMA) and P150 (rest of the country)</h2>
          </div>
        </div>
    </div>
  </div>
</div>
<?php if(isset($success)) { ?>
    <script type="text/javascript">
      alert('Signup Complete, Please wait for Approval');
    </script>
<?php } ?>