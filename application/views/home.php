<div class="page-header" data-parallax="true" style="background-image: url('<?=base_url()?>assets/img/image001.jpg');">
            <div class="filter"></div>
            <div class="container">
                <div class="motto text-center">
                    <img src="<?=base_url()?>assets/img/leap-logo-only.png">
                    <h1 style="font-weight:500;font-size:4.6em">LEAP, Inc.</h1>
                    <h3>Literature Educators Association of the Philippines, Inc.</h3>
                    <br>

                </div>
            </div>
        </div>

        <div class="main">
            <div class="section text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title text-center">News & Updates</h2>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image">
                                    <a>
                                        <img class="d-block img-fluid" src="assets/img/homepage/image007.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h3 class="card-title">
                                        <a>NCLE 2017 Plenary Lecture delivered </a>
                                    </h3>
                                    <p class="card-description">
                                        by Dr. Venancio L. Mendiola
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image">
                                    <a>
                                        <img class="d-block img-fluid" src="assets/img/homepage/image005.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h3 class="card-title">
                                        <a>Keynote Speaker opened NCLE 2017</a>
                                    </h3>
                                    <p class="card-description">
                                       Prof. Ruth A. Alido opened NCLE 2017 with an empowering talk on the role of teachers in the effective implementation of the K to 12 curriculum.
                                    </p>
                                </div>
                            </div>
                        </div>               
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image">
                                    <a href="#pablo">
                                        <img class="d-block img-fluid" src="assets/img/homepage/image010.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image">
                                    <a href="#pablo">
                                        <img class="d-block img-fluid" src="assets/img/homepage/image012.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <h4 class="title mr-auto ml-auto">Literature educators actively showed their participation in the panel discussion portion of the NCLE.</h4>
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image mr-auto ml-auto">
                                    <a>
                                        <img class="d-block img-fluid" src="assets/img/homepage/image014.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-plain card-blog text-center">
                                <div class="card-image mr-auto ml-auto">
                                    <a>
                                        <img class="d-block img-fluid" src="assets/img/homepage/image016.jpg">
                                    </a>
                                </div>
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="section section-dark text-center">
            <div class="container">
                <h2 class="title">President's Corner</h2>
                <div class="row">




                    <div class="col-md-12">
                        <div class="card card-profile card-plain">
                            <div class="card-avatar">
                                <a href="#avatar"><img src="<?=base_url()?>assets/img/faces/jennie.jpg" alt="..."></a>
                            </div>
                            <div class="card-body">
                                <a href="#paper-kit">
                                    <div class="author">
                                        <h4 class="card-title">JENNIE V. JOCSON, Ph.D. </h4>
                                        <h6 class="card-category">(Philippine Normal University) <br>
                                    President</h6>
                                    </div>
                                </a>
                                <p class="card-description text-center">
                                
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="section landing-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="text-center">What are you waiting for? Sign Up!</h2>
                            <!--<form class="contact-form" method="POST" action="<?=base_url()?>user/register_member">
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>First Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-single-02"></i>
                                            </span>
                                            <input type="text" name="firstname" class="form-control" placeholder="Juan">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Last Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-single-02"></i>
                                            </span>
                                            <input type="text" name="lastname" class="form-control" placeholder="Dela Cruz">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-email-85"></i>
                                            </span>
                                            <input type="text" name="email_address" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ml-auto mr-auto">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="nc-icon nc-key-25"></i>
                                            </span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="visibility: hidden; display: none;">
                                    <div class="col-md-12  ml-auto mr-auto">
                                            <div class="text-center">
            
                                                <select name="membership_type" class="selectpicker text-center col-md-6" data-style="btn-leap btn-round" data-menu-style="dropdown-info">
                                                    <option disabled > Pick membership</option>
                                                    <option value="1" selected>Regular </option>
                                                    <!--<option value="2">Student</option>
                                                    <option value="3">Insitutional</option>
                                                    <option value="4">Lifetime </option>  -->  
                                                </select>
                                            </div>  
                                           
                                    </div>
                                </div>
                                
                                
                            </form>-->
                            <div class="row">
                                <div class="col-md-1 ml-auto mr-auto">
                                    <a href="<?=base_url()?>sign_up/regular_sign_up" class="btn btn-leap btn-lg btn-fill">Sign Up</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
