<div class="main">
  	<div class="features-1">
	    <div class="container">
	        <div class="row">
	        	<div class="col-md-8 ml-auto mr-auto text-center">
            		<h2 class="title">Membership Tracking</h2>
            		<h5 class="description"> Here are the list of approved membership accounts </h5>
          		</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-8 mr-auto ml-auto text-center">
	        		<div class="row">
	        			<div class="col-md-6"  style="margin-bottom: 20px">
	        				NAME
	        			</div>
	        			<div class="col-md-6"  style="margin-bottom: 20px">
	        				MEMBERSHIP TYPE
	        			</div>
	        			<?php foreach($approved_members->result() as $approved_member) { ?>
		        			<div class="col-md-6">
	        					<?=$approved_member->firstname?> <?=$approved_member->middlename?> <?=$approved_member->lastname?>
		        			</div>
		        			<div class="col-md-6">
		        				<?php if($approved_member->membership_type == 1) { ?>
		        					Regular Membership
		        				<?php } else if($approved_member->membership_type == 2) { ?>
		        					Student Membership
	        					<?php } else if($approved_member->membership_type == 3) { ?>
	        						Institutional Membership
	        					<?php } else if($approved_member->membership_type == 4) { ?>
	        						Lifetime Membership
	        					<?php } ?>
		        			</div>
	        			<?php } ?>
	        		</div>
	        	</div>		        	
	        </div>
	       
	    </div>
	</div>
</div>
