
<div class="main">
  <div class="features-1">
    <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Membership shall be classified as follows: </h2>
            <h5 class="description">The Founding Members are the first set of officers, advisers, consultants, and members who registered and paid their registration fee at the organizational meeting held on November 19, 2016 at the Philippine Normal University – Manila. </h5>
          </div>
        </div>
        <div class="row">
            <div class="blog-1" id="blog-1">
              <div class="container">
                <div class="row">
                  <div class="col-md-10 ml-auto mr-auto">
                    <div class="card card-plain card-blog">
                      <!-- REGULAR -->
                      <div class="row ">
                        <div class="col-md-8 ml-auto mr-auto">
                          <div class="card-body">
                            <h6 class="card-category text-info"></h6>
                            <h3 class="card-title">
                              <a>The Regular Members</a>
                            </h3>
                            <p class="card-description">
                              The Regular Members are professionals from public and private educational institutions and other government and non-government organizations who meet the qualifications set by the association and pay their annual membership fees every year and actively participate in the association’s annual convention/s. 
                            </p>
                          </div>
                        </div>
                      </div>
                      <!-- ENDING OF REGULAR -->
                      <!-- LIFETIME -->
                      <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                          <div class="card-body">
                            <h6 class="card-category text-info"></h6>
                            <h3 class="card-title">
                              <a>The Lifetime Members</a>
                            </h3>
                            <p class="card-description">
                              The Lifetime Members are professionals who apply as lifetime members upon the approval of the Board and who opt to pay the corresponding membership fee. 
                            </p>
                          </div>
                        </div>
                      </div>
                      <!-- ENDING OF LIFETIME -->
                      <!-- INSTITUTIONAL  -->
                      <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                          <div class="card-body">
                            <h6 class="card-category text-info"></h6>
                            <h3 class="card-title">
                              <a >The Institutional Members</a>
                            </h3>
                            <p class="card-description">
                              Lastly, the Institutional Members are institutions and organizations that signify their intention to support the cause of teachers and education by paying the institutional membership fee.
                            </p>
                          </div>
                        </div>
                      </div>
                      <!-- ENDING OF INSTITUTIONAL -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h5 class="description">Members shall register through the Secretariat by filling-out the membership form and paying the corresponding membership fee. </h5>
          </div>
        </div>
    </div>
  </div>
  <div class="blog-2 section section-gray">
    <div class="container">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <h2 class="title text-center">How much is membership?</h2>
          <br />
          <div class="row">
            <!--<div class="col-md-3">
              <div class="card card-blog">
                <div class="card-image">

                </div>
                <div class="card-body">
                  <h6 class="card-category text-info">Student Membership</h6>
                  <h5 class="card-title">
                    <a href="<?=base_url()?>student/sign_up">  Php 700/year</a>
                  </h5>
                  <p class="card-description">

                  </p>
                    <hr>
                    <div class="card-footer">
                        <div class="author">

                      </div>
                      <div class="stats">
                        <a href="<?=base_url()?>student/sign_up"> <i class="fa fa-sign-in" aria-hidden="true"></i> Sign-up Now! </a>
                      </div>
                  </div>
                </div>
              </div>
            </div>-->
            <div class="row">
              <div class="col-md-8 ml-auto mr-auto">
                <div class="card-body">
                  <h6 class="card-category text-info"></h6>
                  <h3 class="card-title">
                    <a> 1.  Founding Members</a>
                  </h3>
                  <p class="card-description">
                   No membership fees will be collected from the founding members as their special privilege. 
                  </p>
                </div>
              </div>

               <div class="col-md-8 ml-auto mr-auto">
                <div class="card-body">
                  <h6 class="card-category text-info"></h6>
                  <h3 class="card-title">
                    <a> 2. Regular Members </a>
                  </h3>
                  <p class="card-description">
                   The annual fee is five hundred pesos only (P500.00), which may be adjusted as needed, subject to the approval of the Board of Directors on its regular meeting. 
                  </p>
                </div>
              </div>

              <div class="col-md-8 ml-auto mr-auto">
                <div class="card-body">
                  <h6 class="card-category text-info"></h6>
                  <h3 class="card-title">
                    <a> 3.  Lifetime Members </a>
                  </h3>
                  <p class="card-description">
                   The fees to be paid will vary according to the following age brackets: 
                  </p>
                </div>
              </div>
            </div>


             <div class="col-md-4">
              <div class="card card-blog">
                <div class="card-image">
                </div>
                <div class="card-body">
                  <h6 class="card-category text-info text-center">Ages 20-29 </h6>
                  <h5 class="card-title text-center">
                    <a >  Php 10,000</a>
                  </h5>
                  <p class="card-description">

                  </p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-blog">
                <div class="card-image">
                </div>
                <div class="card-body">
                  <h6 class="card-category text-info text-center">Ages 30-39 </h6>
                  <h5 class="card-title text-center">
                    <a >  Php 7,500</a>
                  </h5>
                  <p class="card-description">

                  </p>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card card-blog">
                <div class="card-image">
                </div>
                <div class="card-body">
                  <h6 class="card-category text-info text-center">Ages 40-49 </h6>
                  <h5 class="card-title text-center">
                    <a >  Php 5,000</a>
                  </h5>
                  <p class="card-description">

                  </p>
                </div>
              </div>
            </div>
           
           <div class="col-md-6">
              <div class="card card-blog">
                <div class="card-image">
                </div>
                <div class="card-body">
                  <h6 class="card-category text-info text-center">Ages 50-59 </h6>
                  <h5 class="card-title text-center">
                    <a >  Php 2,500</a>
                  </h5>
                  <p class="card-description">

                  </p>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card card-blog">
                <div class="card-image">
                </div>
                <div class="card-body">
                  <h6 class="card-category text-info text-center">Ages 60 Above </h6>
                  <h5 class="card-title text-center">
                    <a >  Php 1,500</a>
                  </h5>
                  <p class="card-description">

                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <a href="<?=base_url()?>sign_up/regular_sign_up" class="ml-auto mr-auto"><h2 class="title text-center" >Sign-Up Now!</h2></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
