<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->library('template');
		$this->load->model('User_Model');
	}

	public function index()
	{
		if(isset($_SESSION['admin_email']))
		{
			redirect('admin/pending_members');
		}
		else
		{
			$this->load->view('admin/login');
		}
		
	}

	public function verify_login()
	{
		$data = $_POST;
		if ($this->security->xss_clean($data,TRUE) === FALSE)
		{
			echo "not clean";
		}
		else
		{
			$response = $this->User_Model->verify_admin_login($data);
			if ($response->num_rows() > 0)
			{
				$response2=$response->row();
				$_SESSION['admin_email'] = $response2->email;//$response['email'];
				$_SESSION['firstname'] = $response2->firstname;//$response['firstname'];
				$_SESSION['middlename'] =$response2->middlename;// $response['middlename'];
				$_SESSION['lastname'] =$response2->lastname;// $response['lastname'];
				echo "verified";
			}
			else
			{
				echo "Board Member Account not existing";
			}
		}
	}

	public function logout()
	{
		session_destroy();
		redirect('admin');
	}


	public function user_profile()
	{
		$data['link']="admin/user_profile";
		$this->template->load('_admin_layout','admin/user_profile',$data);
	}

	public function pending_members()
	{
		if(isset($_SESSION['admin_email']))
		{
			$data['link']="admin/pending_members";

			$pending_members = $this->User_Model->get_pending_members();
			foreach ($pending_members->result() as $pending_member) 
			{
				$pending_member->fullname = $pending_member->firstname." ".$pending_member->middlename." ".$pending_member->lastname;
			}
			$data['pending_members'] = $pending_members;
			$this->template->load('_admin_layout','admin/pending_members',$data);
		}
		else
		{
			redirect('admin');
		}
	}

	public function approved_members()
	{
		if(isset($_SESSION['admin_email']))
		{
			$data['link']="admin/approved_members";

			$approved_members = $this->User_Model->get_approved_members();
			foreach ($approved_members->result() as $approved_member) 
			{
				$approved_member->fullname = $approved_member->firstname." ".$approved_member->middlename." ".$approved_member->lastname;
			}
			$data['approved_members'] = $approved_members;
			$this->template->load('_admin_layout','admin/approved_members',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	public function sample()
	{
		print_r($_SESSION);
	}
}
