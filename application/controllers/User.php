<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model('User_model');
	}

	public function register_member()
	{
		$data = $_POST;
		$email_check = $this->User_model->check_email_existing($data);
		if($email_check->num_rows() > 0)
		{
			redirect('sign_up/form/'.'existing_email');
		}
		else
		{
			
			$data['specialization'] = json_encode($data['specialization']);
			$data['interest'] = json_encode($data['interest']);
			$response = $this->User_model->register_member($data);
			if ($response="SUCCESS")
			{
				extract($data);
				if($membership_type == "1")
				{
					redirect('sign_up/regular/success');
				}
				elseif ($membership_type == "2") 
				{
					redirect('student');
				}
				elseif ($membership_type == "3") 
				{
					redirect('institutional');
				}
				elseif ($membership_type == "4") 
				{
					redirect('lifetime');
				}
			}
			else
			{
				redirect('sign_up');
			}
		}		
	}

	public function approve_member($user_id)
	{
		if(isset($_SESSION['admin_email']))
		{
			$response = $this->User_model->approve_member($user_id);
			if($response == "SUCCESS")
			{
				redirect('admin/approved_members');
			}
			else
			{
				redirect('admin/pending_members');
			}
		}
		else
		{

		}
	}

}
