<?php if( !defined('BASEPATH')) exit('No direct script access allowed');

class Sign_up extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('User_Model');
	}


	public function form($response="")
	{
		if(!is_null($response))
		{
			$data['response'] = $response;
			$this->load->view('register',$data);
		}
		else
		{
			$this->load->view('register');
		}
	}

	public function overview()
	{
		$this->template->load('_layout','overview');
	}

	public function regular($success=NULL)
	{
		$data['success']=$success;
		$this->template->load('_layout','regular_overview',$data);
	}
	
	public function student()
	{
		$this->template->load('_layout','student_overview');
	}

	public function institutional()
	{
		$this->template->load('_layout','institutional_overview');
	}

	public function lifetime()
	{
		$this->template->load('_layout','lifetime_overview');
	}

	public function regular_sign_up()
	{
		$this->template->load('_layout','regular_signup');
	}

	public function student_sign_up()
	{
		$this->template->load('_layout','student_signup');
	}

	public function institutional_sign_up()
	{
		$this->template->load('_layout','institutional_signup');
	}

	public function lifetime_sign_up()
	{
		$this->template->load('_layout','lifetime_signup');
	}

	public function approved_members()
	{
		$data['approved_members'] = $this->User_Model->get_approved_members();
		$this->template->load('_layout','membership_tracking',$data);
	}
}
