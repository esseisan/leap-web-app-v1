<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

	/*public function save_user($data)
	{
		extract($data);
		$sql = "INSERT INTO sample (username,password) VALUES ('$email','$password')";
		$this->db->query($sql);
		return "SUCCESS";
	}*/

	public function register_member($data)
	{
		extract($data);
		$query = "INSERT INTO user (firstname,middlename,lastname,gender,birthday,email_address,highest_degree,school,job_position,taught_level,teaching_years,specialization,interest,address,residence_number,office_number,send_membership,leap_password,membership_type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->db->query($query,array($firstname,$middlename,$lastname,$gender,$birthdate,$email_address,$highest_degree,$school,$job_position,$taught_level,$teaching_years,$specialization,$interest,$address,$residence_number,$office_number,$send_membership,md5($password),$membership_type));
		return "SUCCESS";
	}

	public function check_email_existing($data)
	{
		extract($data);
		$query = "SELECT * FROM user WHERE email_address = ?";
		$result = $this->db->query($query,array($email_address));
		return $result;
	}

	public function get_pending_members()
	{
		$query="SELECT * FROM user WHERE approved = 0";
		$result = $this->db->query($query);
		return $result;
	}

	public function approve_member($user_id)
	{
		$date=date('F j, Y');
		$query="UPDATE user SET approved = 1,date_approved  = ? WHERE user_id = ?";
		$this->db->query($query,array($date,$user_id));
		return "SUCCESS";
	}

	public function get_approved_members()
	{
		$query="SELECT * FROM user WHERE approved = 1";
		$result = $this->db->query($query);
		return $result;
	}

	public function get_approved_members_limit($limit,$pagination)
	{
		$pagination_limit = ($pagination * $limit) - $limit;
		$query="SELECT * FROM user WHERE approved = 1 LIMIT ?,?";
		$result = $this->db->query($query,array($pagination_limit,$limit));
		return $result;
	}


	public function verify_admin_login($data)
	{
		extract($data);
		$query="SELECT * FROM admin WHERE email = ? AND password = ?";
		$result = $this->db->query($query,array($email_address,md5($password)));
		return $result;
	}
}
