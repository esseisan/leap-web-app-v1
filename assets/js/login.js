
$(document).on("click",".login-btn",function()
{
	let email_address = $("#email_address").val();
	let password = $("#password").val();
	$.post("admin/verify_login",{email_address:email_address,password:password},function(response){
		if(response == "verified")
		{
			window.location.replace('admin/pending_members');
		}
		else
		{
			$("#exampleModalLabel").text('Error Logging In');
			$(".modal-body").text(response);
			$("#EmailErrorModal").modal('show');
		}
	});
	return false;
});

