$('#datetimepicker').datetimepicker({
                 format: 'MM/DD/YYYY'
           });


$( "input" ).change(function() {
var buttons = jQuery("input:radio:checked").get();
    var values = $.map(buttons, function(element) {
        return $(element).attr("value");
    });
    
});



$('#specialization_other').click(function(){
	if($(this).is(':checked'))
	{
		document.getElementById("specialization_textarea").style.visibility = "initial";
		$("#specialization_textarea").attr("required",true);
	}
	else
	{
		document.getElementById("specialization_textarea").style.visibility = "hidden";
		$("#specialization_textarea").attr("required",false);
	}
});

$('#interest_other').click(function(){
	if($(this).is(':checked'))
	{
		document.getElementById("interest_textarea").style.visibility = "initial";
		$("#interest_textarea").attr("required",true);
	}
	else
	{
		document.getElementById("interest_textarea").style.visibility = "hidden";
		$("#interest_textarea").attr("required",false);
	}
});